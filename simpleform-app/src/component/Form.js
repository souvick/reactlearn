import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";

function Form() {
  const [countryName, setCountryName] = useState("");
  const [cityList, setCityList] = useState([]);

  const handleSubmit = (evt) => {
    evt.preventDefault();
  };

  useEffect(() => {
    fetch(
      `https://api.openaq.org/v1/cities?country=${countryName.toUpperCase()}`,
      {
        method: "GET",
        headers: new Headers({
          Accept: "application/json",
        }),
      }
    )
      .then((res) => res.json())
      .then((response) => {
        setCityList(response.results);
      })
      .catch((error) => console.log(error));
  }, [countryName]);

  return (
    <div className="container">
      <form className="form-group">
        <label>
          Country Code:
          <input
            type="text"
            className="form-control"
            value={countryName}
            onChange={(e) => setCountryName(e.target.value)}
          />
        </label>
        <br />
        <br />
      </form>

      {cityList
        ? cityList.map((city) => {
            return <div key={city.name}>{city.name}</div>;
          })
        : ""}
    </div>
  );
}

export default Form;
