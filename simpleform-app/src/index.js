import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Route, Link, Switch, BrowserRouter as Router } from "react-router-dom";
import Form from "./component/Form";

const routing = (
  <Router>
    <Switch>
      <Route exact path="/" component={App}></Route>
      <Route path="/form" component={Form}></Route>
    </Switch>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));
