import React, { useState } from "react";
import Form from "./component/Form";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="container-fluid">
      <div className="alert alert-primary">
        <h1>City List API - Type two digit country code</h1>
      </div>
      <div className="row">
        <Form></Form>
      </div>
    </div>
  );
}

export default App;
